package edu.usu.csse.encrypt;

import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class AsymmetricCipherTest {

    @Test
    public void checkConstruction() {
        AsymmetricCipher cipher = new AsymmetricCipher();
        assertTrue(cipher.isValid());
    }

    @Test
    public void checkAsymmetricEncryptionWithPublicKey() throws Exception {

        AsymmetricCipher cipher = new AsymmetricCipher();

        PublicKeyReader publicKeyReader1 = new PublicKeyReader();
        PublicKey publicKey1 = publicKeyReader1.readFromResource("/test1_public_key.pem");
        assertNotNull(publicKey1);

        byte[] plainData = "This is a test message".getBytes();

        byte[] encryptedData = cipher.encrypt(publicKey1, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        PrivateKeyReader privateKeyReader1 = new PrivateKeyReader();
        PrivateKey privateKey1 = privateKeyReader1.readFromResource("/test1_private_key.pem","test1");
        assertNotNull(privateKey1);


        byte[] decryptedData = cipher.decrypt(privateKey1, encryptedData);
        assertNotNull(decryptedData);
        assertTrue(decryptedData.length>0);
        assertArrayEquals(plainData, decryptedData);


        PrivateKeyReader privateKeyReader2 = new PrivateKeyReader();
        PrivateKey privateKey2 = privateKeyReader2.readFromResource("/test2_private_key.pem","test2");
        assertNotNull(privateKey2);

        PublicKeyReader publicKeyReader2 = new PublicKeyReader();
        PublicKey publicKey2 = publicKeyReader2.readFromResource("/test2_public_key.pem");
        assertNotNull(publicKey2);

        plainData = "This is another test message".getBytes();

        encryptedData = cipher.encrypt(publicKey2, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        decryptedData = cipher.decrypt(privateKey2, encryptedData);
        assertNotNull(decryptedData);
        assertTrue(decryptedData.length>0);
        assertArrayEquals(plainData, decryptedData);

        try {
            cipher.decrypt(privateKey1, encryptedData);
            fail("An exception should have been throw");
        }
        catch (EncryptionException e) {
            assertEquals("Decryption error", e.getMessage());
        }
    }

    @Test
    public void checkAsymmetricEncryptionWithPrivateKey() throws Exception {
        AsymmetricCipher cipher = new AsymmetricCipher();
        
        PrivateKeyReader privateKeyReader1 = new PrivateKeyReader();
        PrivateKey privateKey1 = privateKeyReader1.readFromResource("/test1_private_key.pem","test1");
        assertNotNull(privateKey1);

        byte[] plainData = "This is a test message".getBytes();

        byte[] encryptedData = cipher.encrypt(privateKey1, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        PublicKeyReader publicKeyReader1 = new PublicKeyReader();
        PublicKey publicKey1 = publicKeyReader1.readFromResource("/test1_public_key.pem");
        assertNotNull(publicKey1);

        byte[] decryptedData = cipher.decrypt(publicKey1, encryptedData);
        assertNotNull(decryptedData);
        assertTrue(decryptedData.length>0);
        assertArrayEquals(plainData, decryptedData);


        PrivateKeyReader privateKeyReader2 = new PrivateKeyReader();
        PrivateKey privateKey2 = privateKeyReader2.readFromResource("/test2_private_key.pem","test2");
        assertNotNull(privateKey2);

        plainData = "This is another test message".getBytes();

        encryptedData = cipher.encrypt(privateKey2, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        PublicKeyReader publicKeyReader2 = new PublicKeyReader();
        PublicKey publicKey2 = publicKeyReader2.readFromResource("/test2_public_key.pem");
        assertNotNull(publicKey2);

        decryptedData = cipher.decrypt(publicKey2, encryptedData);
        assertNotNull(decryptedData);
        assertTrue(decryptedData.length>0);
        assertArrayEquals(plainData, decryptedData);

        try {
            cipher.decrypt(privateKey1, encryptedData);
            fail("An exception should have been throw");
        }
        catch (EncryptionException e) {
            assertEquals("unable to decrypt block", e.getMessage());
        }
    }

    @Test
    public void checkAsymmetricEncryptionWithNoKeys() throws Exception {
        AsymmetricCipher cipher = new AsymmetricCipher();

        PrivateKeyReader privateKeyReader1 = new PrivateKeyReader();
        PrivateKey privateKey1 = privateKeyReader1.readFromResource("/test1_private_key.pem","test1");
        assertNotNull(privateKey1);

        PublicKeyReader publicKeyReader1 = new PublicKeyReader();
        PublicKey publicKey1 = publicKeyReader1.readFromResource("/test1_public_key.pem");
        assertNotNull(publicKey1);

        byte[] plainData = "This is a test message".getBytes();

        try {
            cipher.encrypt(null, plainData);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No key provided", e.getMessage());
        }

        byte[] encryptedData = cipher.encrypt(publicKey1, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        try {
            cipher.decrypt(null, encryptedData);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No key provided", e.getMessage());
        }

    }

    @Test
    public void checkAsymmetricEncryptionNoData() throws Exception {
        AsymmetricCipher cipher = new AsymmetricCipher();

        PrivateKeyReader privateKeyReader1 = new PrivateKeyReader();
        PrivateKey privateKey1 = privateKeyReader1.readFromResource("/test2_private_key.pem","test2");
        assertNotNull(privateKey1);

        PublicKeyReader publicKeyReader1 = new PublicKeyReader();
        PublicKey publicKey1 = publicKeyReader1.readFromResource("/test2_public_key.pem");
        assertNotNull(publicKey1);

        try {
            cipher.encrypt(publicKey1, null);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

        try {
            cipher.encrypt(publicKey1, new byte[] {});
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

        try {
            cipher.decrypt(publicKey1, null);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

        try {
            cipher.decrypt(publicKey1, new byte[] {});
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }
    }

    @Test
    public void checkWrongTypeOfKeyInPem() throws Exception {
        PrivateKeyReader privateKeyReader = new PrivateKeyReader();

        assertThrows(EncryptionException.class, () -> {
            privateKeyReader.readFromResource("/test2_public_key.pem","test2");
        });

    }

}