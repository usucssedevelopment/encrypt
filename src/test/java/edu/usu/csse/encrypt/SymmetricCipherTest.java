package edu.usu.csse.encrypt;

import javax.crypto.SecretKey;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class SymmetricCipherTest {
    @Test
    public void checkConstruction() {
        SymmetricCipher cipher = new SymmetricCipher();
        assertTrue(cipher.isValid());
    }

    @Test
    public void checkGetSecretKey() throws Exception {
        SymmetricCipher cipher = new SymmetricCipher();

        SecretKey secretKey = cipher.generateKey();
        assertNotNull(secretKey);
        assertEquals("AES", secretKey.getAlgorithm());
    }

    @Test
    public void checkSymEncryptionForNormalMessage() throws Exception {
        SymmetricCipher cipher = new SymmetricCipher();

        SecretKey secretKey = cipher.generateKey();
        byte[] plainData = "This is a test message".getBytes();


        byte[] encryptedData = cipher.encrypt(secretKey, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        byte[] decryptedData = cipher.decrypt(secretKey, encryptedData);
        assertNotNull(decryptedData);
        assertTrue(decryptedData.length>0);
        assertArrayEquals(plainData, decryptedData);
    }

    @Test
    public void checkWithNoKey() throws Exception {
        SymmetricCipher cipher = new SymmetricCipher();
        SecretKey secretKey = cipher.generateKey();

        byte[] plainData = "This is a test message".getBytes();

        try {
            cipher.encrypt(null, plainData);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No key provided", e.getMessage());
        }

        byte[] encryptedData = cipher.encrypt(secretKey, plainData);
        assertNotNull(encryptedData);
        assertTrue(encryptedData.length>0);

        try {
            cipher.decrypt(null, plainData);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No key provided", e.getMessage());
        }

    }

    @Test
    public void checkWithNoData() throws Exception {
        SymmetricCipher cipher = new SymmetricCipher();

        SecretKey secretKey = cipher.generateKey();

        try {
            cipher.encrypt(secretKey, null);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

        try {
            cipher.encrypt(secretKey, new byte[]{});
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

        try {
            cipher.decrypt(secretKey, null);
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

        try {
            cipher.decrypt(secretKey, new byte[]{});
            fail("Expected exception not thrown");
        }
        catch (EncryptionException e) {
            assertEquals("No data provided", e.getMessage());
        }

    }

}