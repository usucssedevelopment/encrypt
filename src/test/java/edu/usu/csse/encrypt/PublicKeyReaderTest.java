package edu.usu.csse.encrypt;

import org.junit.jupiter.api.Test;
import java.security.PublicKey;
import static org.junit.jupiter.api.Assertions.*;


public class PublicKeyReaderTest {
    @Test
    public void checkReadFromResource() throws Exception {

        PublicKeyReader publicKeyReader = new PublicKeyReader();
        PublicKey key = publicKeyReader.readFromResource("/test1_public_key.pem");
        assertNotNull(key);

        key = publicKeyReader.readFromResource("/test2_public_key.pem");
        assertNotNull(key);
    }

    @Test
    public void checkReadFromFile() throws Exception {

        PublicKeyReader publicKeyReader = new PublicKeyReader();
        PublicKey key = publicKeyReader.readFromFile("build/resources/test/test1_public_key.pem");
        assertNotNull(key);

        key = publicKeyReader.readFromFile("build/resources/test/test2_public_key.pem");
        assertNotNull(key);
    }

    @Test
    public void checkWrongTypeOfPemFromResource() throws Exception {

        try {
            PublicKeyReader publicKeyReader = new PublicKeyReader();
            publicKeyReader.readFromResource("/test1_private_key.pem");
            fail("Expected an exception");
        } catch (Exception e) {
            assertEquals(CorruptedKey.class, e.getClass());
        }

    }

    @Test
    public void checkWrongTypeOfPemFromFile() throws Exception {

        try {
            PublicKeyReader publicKeyReader = new PublicKeyReader();
            publicKeyReader.readFromFile("build/resources/test/test1_private_key.pem");
            fail("Expected an exception");
        } catch (Exception e) {
            assertEquals(CorruptedKey.class, e.getClass());
        }

    }

    @Test
    public void checkMissingPublicKeyResource() throws Exception {

        try {
            PublicKeyReader publicKeyReader = new PublicKeyReader();
            publicKeyReader.readFromResource("/nonexistent.pem");
            fail("Expected an exception");
        } catch (Exception e) {
            assertEquals(NonExistentKeyException.class, e.getClass());
        }
    }

    @Test
    public void checkMissingPublicKeyFile() throws Exception {

        try {
            PublicKeyReader publicKeyReader = new PublicKeyReader();
            publicKeyReader.readFromFile("nonexistent.pem");
            fail("Expected an exception");
        } catch (Exception e) {
            assertEquals(NonExistentKeyException.class, e.getClass());
        }
    }

    @Test
    public void checkBadPublicKeyResource() throws Exception {
        try {
            PublicKeyReader publicKeyReader = new PublicKeyReader();
            publicKeyReader.readFromResource("/bad_public_key.pem");
            fail("Expected an exception");
        } catch (Exception e) {
            assertEquals(CorruptedKey.class, e.getClass());
        }
    }

    @Test
    public void checkBadPublicKeyFile() throws Exception {
        try {
            PublicKeyReader publicKeyReader = new PublicKeyReader();
            publicKeyReader.readFromFile("build/resources/test/bad_public_key.pem");
            fail("Expected an exception");
        } catch (Exception e) {
            assertEquals(CorruptedKey.class, e.getClass());
        }
    }
}