package edu.usu.csse.encrypt;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EncryptionExceptionTest {

    @Test
    public void checkEverything() {
        EncryptionException exception = new EncryptionException();
        assertEquals("Unknown encryption exception", exception.getMessage() );

        exception = new EncryptionException("Test error message");
        assertEquals("Test error message", exception.getMessage() );
    }
}