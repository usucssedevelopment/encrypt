package edu.usu.csse.encrypt;

import org.bouncycastle.util.encoders.DecoderException;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.security.PrivateKey;


public class PrivateKeyReaderTest {
    @Test
    public void checkPrivateKeyResource() throws Exception {
        PrivateKeyReader privateKeyReader = new PrivateKeyReader();
        PrivateKey privateKey1 = privateKeyReader.readFromResource("/test1_private_key.pem","test1");
        assertNotNull(privateKey1);

        PrivateKey privateKey2 = privateKeyReader.readFromResource("/test2_private_key.pem","test2");
        assertNotNull(privateKey2);

    }

    @Test
    public void checkPrivateKeyFile() throws Exception {
        PrivateKeyReader privateKeyReader = new PrivateKeyReader();
        PrivateKey privateKey1 = privateKeyReader.readFromFile("build/resources/test/test1_private_key.pem","test1");
        assertNotNull(privateKey1);

        PrivateKey privateKey2 = privateKeyReader.readFromFile("build/resources/test/test2_private_key.pem","test2");
        assertNotNull(privateKey2);

    }

    @Test
    public void checkWrongTypeOfPemFromResource() throws Exception {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromResource("/test2_public_key.pem","test2");
            fail("Expected an exception");
        }
        catch (EncryptionException e)
        {
            assertEquals(CorruptedKey.class, e.getClass());
        }
    }

    @Test
    public void checkWrongTypeOfPemFromFile() throws Exception {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromFile("build/resources/test/test2_public_key.pem","test2");
            fail("Expected an exception");
        }
        catch (EncryptionException e)
        {
            assertEquals(CorruptedKey.class, e.getClass());
        }
    }

    @Test
    public void checkMissingPrivateKeyResource() throws Exception {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromResource("/nonexistent.pem","test1");
            fail("Expected an exception");
        }
        catch (NonExistentKeyException e)
        {
            assertEquals(NonExistentKeyException.class, e.getClass());
        }
    }

    @Test
    public void checkMissingPrivateKeyFile() throws Exception {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromFile("nonexistent.pem","test1");
            fail("Expected an exception");
        }
        catch (NonExistentKeyException e)
        {
            assertEquals(NonExistentKeyException.class, e.getClass());
        }
    }

    @Test
    public void checkBadPasswordPrivateKeyFromResource() {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromResource("/test2_private_key.pem","bad");
            fail("Expected an exception");
        }
        catch (Exception e) {
            assertEquals(IncorrectPasswordException.class, e.getClass());
        }
    }

    @Test
    public void checkBadPasswordPrivateKeyFromFile() {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromFile("build/resources/test/test1_private_key.pem","bad");
            fail("Expected an exception");
        }
        catch (Exception e) {
            assertEquals(IncorrectPasswordException.class, e.getClass());
        }
    }

    @Test
    public void checkBadPrivateKeyFromResource() {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromResource("/bad_private_key.pem","test2");
            fail("Expected an exception");
        }
        catch (Exception e) {
            assertEquals(CorruptedKey.class, e.getClass());
        }
    }

    @Test
    public void checkBadPrivateKeyFromFile() {

        try
        {
            PrivateKeyReader privateKeyReader = new PrivateKeyReader();
            privateKeyReader.readFromFile("build/resources/test/bad_private_key.pem","test2");
            fail("Expected an exception");
        }
        catch (Exception e) {
            assertEquals(CorruptedKey.class, e.getClass());
        }
    }

}