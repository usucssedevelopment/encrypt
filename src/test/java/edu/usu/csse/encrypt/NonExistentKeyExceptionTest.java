package edu.usu.csse.encrypt;

//import edu.usu.csse.encrypt.NonExistentKeyException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class NonExistentKeyExceptionTest {

    @Test
    public void checkEverything() throws Exception {
        NonExistentKeyException ex = new NonExistentKeyException();
        assertEquals("Non-existent Key", ex.getMessage());
    }
}