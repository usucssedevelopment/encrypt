package edu.usu.csse.encrypt;

import edu.usu.csse.encrypt.IncorrectPasswordException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class IncorrectPasswordExceptionTest {

    @Test
    public void checkEverything() throws Exception {
        IncorrectPasswordException ex = new IncorrectPasswordException();
        assertEquals("Invalid Password", ex.getMessage());
    }
}