package edu.usu.csse.encrypt;

/**
 * EncryptionException
 *
 * A specialization of Exception for handing encryption errors.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
public class EncryptionException extends Exception {
    public EncryptionException() { super("Unknown encryption exception"); }

    public EncryptionException(String message) {
        super(message);
    }
}
