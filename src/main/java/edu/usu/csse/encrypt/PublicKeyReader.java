package edu.usu.csse.encrypt;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import java.security.PublicKey;

/**
 * A PublicKeyReader object can read a PEM file created by openssl with
 * a public key.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
@SuppressWarnings("WeakerAccess")
public class PublicKeyReader extends KeyReader {

    public PublicKey readFromResource(String resourceName) throws EncryptionException {
        PEMParser pemParser = openPEM(resourceName);
        return loadKey(pemParser);
    }

    public PublicKey readFromFile(String path) throws EncryptionException {
        PEMParser pemParser = openFromFile(path);
        return loadKey(pemParser);
    }

    private PublicKey loadKey(PEMParser pemParser) throws EncryptionException {
        PublicKey publicKey;

        try {
            SubjectPublicKeyInfo pub = SubjectPublicKeyInfo.getInstance(pemParser.readObject());
            JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
            publicKey = converter.getPublicKey(pub);
        }
        catch (IllegalArgumentException e) {
            throw new CorruptedKey();
        }
        catch (Exception e) {
            throw new EncryptionException(e.getMessage());
        }
        return publicKey;
    }
}
