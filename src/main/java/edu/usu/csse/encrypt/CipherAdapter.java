package edu.usu.csse.encrypt;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.Provider;
import java.security.Security;

/**
 * Cipher Adapter
 *
 * This is general adapter for ciphers that use Bouncy Castle and either symmetric or asymmetric encryption.
 *
 * To use, a client would create either an instance of SymmetricCipher or AsymmetricCipher and then call
 * either encrypt and decrypt with appropriate keys.  Note that SymmetricCipher also include a method for
 * creating a symmetric key.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */

public abstract class CipherAdapter {
    private static boolean hasSetupBeenCalled = false;
    static Provider bcProvider;
    Cipher cipher;
    String cipherError;

    public boolean isValid() {
        setup();
        return cipher!=null && cipherError==null;
    }

    /**
     * Setup encryption provider.  This method must be called before any encryption is performed.
     */
    static void setup() {
        if (hasSetupBeenCalled) return;

        bcProvider = new org.bouncycastle.jce.provider.BouncyCastleProvider();

        Security.addProvider(bcProvider);
        hasSetupBeenCalled = true;
    }

    /**
     * Valid existence of key and data
     *
     * @param key                   Key to be used in the encrypt or decrypt action
     * @param data                  Key to be encrypted or decrypted
     * @throws EncryptionException  Thrown is no key or data provided
     */
    private void validate(Key key, byte[] data) throws EncryptionException {
        if (key==null)
            throw new EncryptionException("No key provided");

        if (data==null || data.length==0)
            throw new EncryptionException("No data provided");
    }

    /**
     *
     * @param key                           The key to be used for the encryption
     * @param plainData                     The data to be encrypted
     * @return                              If successful, the encrypted data; otherwise null
     * @throws EncryptionException          Thrown if the key or data are missing, or if there was a problem with the
     *                                      encryption.
     */
    @SuppressWarnings("WeakerAccess")
    public byte[] encrypt(Key key, byte[] plainData) throws EncryptionException {
        validate(key, plainData);

        byte[] encryptedData;

        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encryptedData = cipher.doFinal(plainData);
        }
        catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            throw new EncryptionException(e.getMessage());
        }

        return encryptedData;
    }

    /**
     *
     * @param key                           The key to be use for the decryption
     * @param encryptedData                 The encrypted data
     * @return                              If successful, the plain data; otherwise null
     * @throws EncryptionException          Thrown if the key or data are missing, or if there was a problem with the
     *                                      decryption.
     */
    @SuppressWarnings("WeakerAccess")
    public byte[] decrypt(Key key, byte[] encryptedData) throws EncryptionException
    {
        validate(key, encryptedData);

        byte[] plainData;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            plainData = cipher.doFinal(encryptedData);
        }
        catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            throw new EncryptionException(e.getMessage());
        }

        return plainData;
    }
}
