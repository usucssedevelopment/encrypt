package edu.usu.csse.encrypt;

/**
 * CorruptedKey
 *
 * A specialization of EncryptionException for handing errors related to corrupted keys (file resources or files).
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
public class CorruptedKey extends EncryptionException {
    public CorruptedKey() { super("Corrupted"); }
    public CorruptedKey(String message) { super(message); }
}
