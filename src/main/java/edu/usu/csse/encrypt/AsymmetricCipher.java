package edu.usu.csse.encrypt;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;

/**
 * ASymmetricCipher
 *
 * An adapter for an asymmetric cipher that inherits from CipherAdapter.  It is useful for doing asymmetric
 * encryption.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
@SuppressWarnings("WeakerAccess")
public final class AsymmetricCipher extends CipherAdapter {

    public AsymmetricCipher() {
        setup();
        try {
            cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            cipherError = e.getMessage();
        }
    }

}
