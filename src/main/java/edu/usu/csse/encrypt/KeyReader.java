package edu.usu.csse.encrypt;

import java.io.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.bouncycastle.openssl.PEMParser;

/**
 * A KeyReader object can read a public or private from PEM file created by openssl.  The private key needs to be
 * a PKCS8 encrypted key.  Here are the openssl commands to create the keys:
 *
 * openssl genrsa -out mykey.pem 8192
 * openssl pkcs8 -topk8 -inform PEM -outform PEM -in mykey.pem -out private_key.pem
 * openssl rsa -in mykey.pem -pubout -outform PEM -out public_key.pem
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
@SuppressWarnings("WeakerAccess")
public abstract class KeyReader {
    private static final Logger log = LogManager.getLogger(KeyReader.class.getName());

    public KeyReader() {
        CipherAdapter.setup();
    }

    protected PEMParser openPEM(String resourceName) throws EncryptionException {
        log.debug("Open PEM from resource: resource={}", resourceName);

        InputStream res = this.getClass().getResourceAsStream(resourceName);
        if (res==null) {
            throw new NonExistentKeyException();
        }

        Reader bufferedReader = new BufferedReader(new InputStreamReader(res));
        return new PEMParser(bufferedReader);
    }

    protected PEMParser openFromFile(String path) throws EncryptionException {
        log.debug("Open PEM from file: pathName={}", path);

        PEMParser pemParser;
        try {
            InputStream inputStream = new FileInputStream(path);
            Reader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            pemParser = new PEMParser(bufferedReader);
        } catch (FileNotFoundException e) {
            throw new NonExistentKeyException();
        }

        return pemParser;
    }
}

