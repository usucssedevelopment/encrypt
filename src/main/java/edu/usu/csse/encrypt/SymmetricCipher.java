package edu.usu.csse.encrypt;

import javax.crypto.*;
import java.security.NoSuchAlgorithmException;

/**
 * SymmetricCipher
 *
 * An adapter for an symmetric cipher that inherits from CipherAdapter.  It is useful for doing symmetric
 * encryption.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */

@SuppressWarnings("WeakerAccess")
public final class SymmetricCipher extends CipherAdapter {

    public SymmetricCipher() {
        setup();
        try {
            cipher = Cipher.getInstance("AES", bcProvider);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            cipherError = e.getMessage();
        }
    }

    /**
     * Get a new symmetric key
     *
     * @return                      The new secret (symmetric) key
     * @throws EncryptionException  Thrown if the AES algorithm is not available
     */
    public SecretKey generateKey() throws EncryptionException {
        SecretKey secretKey;
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES", bcProvider);
            keyGenerator.init(128);
            secretKey = keyGenerator.generateKey();
        }
        catch (NoSuchAlgorithmException e) {
            throw new EncryptionException(String.format("No AES Algorithm: %s", e.getMessage()));
        }
        catch (Exception e) {
            throw new EncryptionException(e.getMessage());
        }
        return secretKey;
    }

}
