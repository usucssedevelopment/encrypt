package edu.usu.csse.encrypt;

/**
 * NonExistentKeyException
 *
 * A specialization of EncryptionException for handing errors related to non-existent keys (bad resource or file names).
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
public class NonExistentKeyException extends EncryptionException {
    public NonExistentKeyException() { super("Non-existent Key"); }
}
