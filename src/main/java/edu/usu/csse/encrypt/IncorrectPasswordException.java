package edu.usu.csse.encrypt;

/**
 * IncorrectPasswordException
 *
 * A specialization of EncryptionException for handing errors related to incorrect passwords for private keys.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
public class IncorrectPasswordException extends EncryptionException {
    public IncorrectPasswordException() { super("Invalid Password"); }
}
