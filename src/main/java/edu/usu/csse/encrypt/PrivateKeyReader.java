package edu.usu.csse.encrypt;

import java.io.*;
import java.security.PrivateKey;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.util.encoders.DecoderException;

/**
 * A PrivateKeyReader object can read a PEM file created by openssl with
 * an encrypted private key.  The private key needs to be a PKCS8 encrypted
 * key.
 *
 * @author      Stephen W. Clyde
 * @version     %I%, %G%
 * @since       1.0
 */
@SuppressWarnings("WeakerAccess")
public final class PrivateKeyReader extends KeyReader {
    private static final Logger log = LogManager.getLogger(PrivateKeyReader.class.getName());

    public PrivateKey readFromResource(String resourceName, String password)
            throws EncryptionException {
        PEMParser pemParser = openPEM(resourceName);
        return loadKey(resourceName, password, pemParser);
    }

    public PrivateKey readFromFile(String path, String password)
            throws EncryptionException {
        PEMParser pemParser = openFromFile(path);
        return loadKey(path, password, pemParser);
    }

    private PrivateKey loadKey(String resourceOrFile, String password, PEMParser pemParser)
            throws EncryptionException {
        PrivateKey privateKey;
        try {
            Object keyObject = pemParser.readObject();
            if (!(keyObject instanceof PKCS8EncryptedPrivateKeyInfo)) {
                log.error("{} does not contains a PKCS8 Encrypted Private Key", resourceOrFile);
                throw new CorruptedKey(String.format("%s does not contains a PKCS8 Encrypted Private Key", resourceOrFile));
            }

            PKCS8EncryptedPrivateKeyInfo encryptedPrivateKeyInfo = (PKCS8EncryptedPrivateKeyInfo) keyObject;
            InputDecryptorProvider provider = new JceOpenSSLPKCS8DecryptorProviderBuilder().build(password.toCharArray());
            PrivateKeyInfo keyInfo= encryptedPrivateKeyInfo.decryptPrivateKeyInfo(provider);

            JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
            privateKey = converter.getPrivateKey(keyInfo);

        }
        catch (CorruptedKey e) {
            throw e;
        }
        catch (IOException e) {
            throw new CorruptedKey(e.getMessage());
        }
        catch (DecoderException e) {
            throw new CorruptedKey();
        }
        catch (PKCSException e) {
            throw new IncorrectPasswordException();
        }
        catch (Exception e) {
            throw new EncryptionException(e.getMessage());
        }

        return privateKey;
    }
}

