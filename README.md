 # encrypt #
  
This is simple library package for symmetric and asymmetric encryption.  It include set of static encryption methods in the
Encryption class, but readers for private and public keys.  Compatible private and public keys can be created using
openssl.

* openssl genrsa -out mykey.pem 8192
* openssl pkcs8 -topk8 -inform PEM -outform PEM -in mykey.pem -out private_key.pem
* openssl rsa -in mykey.pem -pubout -outform PEM -out public_key.pem
 
## Setup of Development Environment ##

To work on this project, you need uses the following tools

    IntelliJ, with
        JDK 1.8.0_231
        The gradle plugin, version 5.2.1

## Development Procedures ##

As you update this project, please do the following:

*  If the version number is not a SNAPSHOT, increment the minor version by one and append a "-SNAPSHOT".
*  Follow good software engineering principles such as Abstraction, Modularity, and Encapsulation.
*  Apply design patterns where appropriate (and only where appropriate)
*  Follow best practices with respect to programming in Java, using Gradle, and with IntelliJ.
*  If at all possible, eliminate all warnings and accept all recommendations made by IDE. If there is a solid
reason to ignore a warning, add an explicit suppression to that warning.
* Avoid all code smells, like long methods, duplicate code, and meaningless names.

## Build Procedures ##

Build the project using Gradle or IntelliJ's build command.

## Testing Procedures ##

After making the desired changes or extension, be sure that all existing test cases run and that you add new
unit, integration, and system test cases to verify the correctness.

## Deployment Procedures ##

For local use, execute the "publishing/publishToMavenLocal" task.

To publish for others, execute the "publishing/publish" task.  When the library is ready for release, remove the
"-SNAPSHOT" from the version number before publishing. Note, when publishing release version, the number must be
unique, so you won't be able to execute the "publishing/publish" task twice with the same version number.
	
## Contribution guidelines ##

* Make sure that there are executable test cases that provide good coverage
* Do a code review with Stephen Clyde or some other administrator assigned to the project.

## Who do I talk to? ##

* Stephen Clyde, Utah State University, Stephen.Clyde@usu.edu, 435-764-1596
